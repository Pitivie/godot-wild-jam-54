extends Button

export var building_name : String
export var building_image : Texture

onready var food = $Building/HBoxContainer/Food
onready var wood = $Building/HBoxContainer/Wood
onready var coin = $Building/HBoxContainer/Coin
onready var building_icon = $Building/BuildingIcon
onready var building_label = $Building/BuildingLabel

func _ready():
	building_label.text = building_name
	building_icon.texture = building_image

func disable(resources):
	disabled = true
	building_icon.modulate = Color('#525252')
	for resource in resources:
		get(resource).disable()

func enable():
	disabled = false
	building_icon.modulate = Color('#ffffff')
	for resource in ["food", "wood", "coin"]:
		get(resource).enable()

func set_costs(costs):
	if costs['food'] == 0:
		food.visible = false
	if costs['wood'] == 0:
		wood.visible = false
	if costs['coin'] == 0:
		coin.visible = false

	food.set_resource(costs['food'])
	wood.set_resource(costs['wood'])
	coin.set_resource(costs['coin'])
