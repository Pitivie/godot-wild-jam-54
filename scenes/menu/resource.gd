extends HBoxContainer
onready var label = $Label
onready var animation_player = $"../AnimationPlayer"

func set_resource(value):
	label.text = str(value)

func disable():
	label.modulate = Color('#ff0000')

func enable():
	label.modulate = Color('#ffffff')
