extends Control

var scenario = [
	[
		"Bonjour Commander.",
		"We finally arrived at the site indicated by our men sent in recognition.",
		"It seems that there is indeed a construction, maybe a temple. This is the first time I see this kind of building here, I did not think the natives capable of such monuments, strange...",
		"Excavation work must begin immediately and recover as much information as necessary but let's be careful, the natives might not appreciate our presence here.",
		"We should be prepared for a possible attack!"
	], 
	[
		"We should be prepared for a possible attack!"	
	],
	[
		"We should be prepared for a possible attack!"
	],
	[
		"We should be prepared for a possible attack!.",
		"We should be prepared for a possible attack!"
	],
	[
		"We should be prepared for a possible attack!",
		"We should be prepared for a possible attack!"
	]
]

var dialogue_index = 0
var scenario_index = 0
var finished = false
var module = null
var station

signal finished

func _ready():
	$Portrait.play("Idle")
#	Global.connect("fragmentExploded", self, "_on_fragment_exploded")
#	Global.connect("oreCollected", self, "_on_ore_collected")
#	station = get_parent().get_parent().get_node("Station")
#	station.connect("docked", self, "_on_station_docked")
#	station.connect("upgraded", self, "_on_station_upgraded")

#	$Transmission.play()
	load_dialogue()


func _process(delta):
	$"Ind".visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		$Portrait.play("Chat")
		load_dialogue()

func _on_fragment_exploded(type, minQty, maxQty, orePosition):
#	Global.disconnect("fragmentExploded", self, "_on_fragment_exploded")
	self.visible = true

#	$Transmission.play()
	$Portrait.play("Chat")
	dialogue_index = 0
	scenario_index = 1
	load_dialogue()

func _on_ore_collected(type, qty):
#	Global.disconnect("oreCollected", self, "_on_ore_collected")
	self.visible = true

#	$Transmission.play()
	dialogue_index = 0
	scenario_index = 2
	load_dialogue()

func _on_station_docked():
	station.disconnect("docked", self, "_on_station_docked")
	self.visible = true

#	$Transmission.play()
	dialogue_index = 0
	scenario_index = 3
	load_dialogue()

func _on_station_upgraded(part):
	station.disconnect("upgraded", self, "_on_station_upgraded")
	self.visible = true

#	$Transmission.play()
	dialogue_index = 0
	scenario_index = 4
	load_dialogue()
	emit_signal("finished")

func load_dialogue():
	if dialogue_index < scenario[scenario_index].size():
		finished = false
		$RichTextLabel.bbcode_text = scenario[scenario_index][dialogue_index]
		$RichTextLabel.percent_visible = 0
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible",0,1,1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$Tween.start()
	else:
		self.visible = false
	dialogue_index += 1



func _on_Tween_tween_completed(object, key):
	finished = true
