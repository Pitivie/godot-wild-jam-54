extends Control
onready var large = $Large
onready var miniature = $miniature

var texture

func _ready():
	large.texture = texture
	miniature.texture = texture

func set_texture(value):
	texture = value

func _on_miniature_mouse_entered():
	large.visible = true

func _on_miniature_mouse_exited():
	large.visible = false
