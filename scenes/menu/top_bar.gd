extends CanvasLayer

onready var food = $Panel/HBoxContainer/Food
onready var wood = $Panel/HBoxContainer/Wood
onready var coin = $Panel/HBoxContainer/Coin



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func update_food(value):
	food.set_resource(value)

func update_wood(value):
	wood.set_resource(value)

func update_coin(value):
	coin.set_resource(value)
