extends CanvasLayer

onready var buildings = $Buildings
onready var build_barracks = $Buildings/HBoxContainer/BuildBarracks
onready var build_tower = $Buildings/HBoxContainer/BuildTower
onready var build_warehouse = $Buildings/HBoxContainer/BuildWarehouse
onready var build_house = $Buildings/HBoxContainer/BuildHouse
onready var head_quarter_units = $HeadQuarterUnits
onready var produce_settler = $HeadQuarterUnits/HBoxContainer/ProduceSettler
onready var settler = $Panel/Settler
onready var settler_counter = $Panel/Settler/Counter

signal building_creation_pressed
signal unit_produced_pressed

func _on_BuildingsButton_pressed():
	buildings.visible = !buildings.visible

func _on_BuildBarracks_pressed():
	emit_signal("building_creation_pressed", "barracks")

func _on_BuildTower_pressed():
	emit_signal("building_creation_pressed", "tower")

func _on_BuildWarehouse_pressed():
	emit_signal("building_creation_pressed", "warehouse")

func _on_BuildHouse_pressed():
	emit_signal("building_creation_pressed", "house")

func set_barracks_costs(costs):
	build_barracks.set_costs(costs)

func set_tower_costs(costs):
	build_tower.set_costs(costs)

func set_warehouse_costs(costs):
	build_warehouse.set_costs(costs)

func set_house_costs(costs):
	build_house.set_costs(costs)

func set_settler_costs(costs):
	produce_settler.set_costs(costs)

func disable_building(building, resources):
	match building:
		"barracks":
			build_barracks.disable(resources)
		"tower":
			build_tower.disable(resources)
		"warehouse":
			build_warehouse.disable(resources)
		"house":
			build_house.disable(resources)

func disable_unit(unit, resources):
	match unit:
		"settler":
			produce_settler.disable(resources)

func toggle_head_quarter_unit_menu():
	head_quarter_units.visible = !head_quarter_units.visible

func _on_ProduceSettler_pressed():
	emit_signal("unit_produced_pressed", "settler")

func _on_SelectAndOrderUnit_selected(number):
	settler.visible = true
	settler_counter.text = str(number)

func _on_SelectAndOrderUnit_unselected():
	settler.visible = false
