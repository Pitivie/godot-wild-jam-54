extends Node2D

onready var spawn = $Spawn

var dragging = false
var selected = []
var drag_start = Vector2.ZERO
var select_rect = RectangleShape2D.new()

signal selected
signal unselected

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and selected.size() > 0 and event.is_pressed():
		for item in selected:
			if item.collider.name != "Obstacles" && item.collider.name != "HeadQuarter" and is_instance_valid(item.collider):
				item.collider.move_to(get_global_mouse_position())
				item.collider.attack_target = null
		spawn.position = get_global_mouse_position()
		spawn.visible = true

	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			spawn.visible = false
			for item in selected:
				if item.collider.name != "Obstacles" && item.collider.name != "HeadQuarter":
					item.collider.unselect()
			emit_signal("unselected")
			selected = []
			dragging = true
			drag_start = get_global_mouse_position()
		elif dragging:
			dragging = false
			update()
			var drag_end = get_global_mouse_position()
			select_rect.extents = (drag_end - drag_start) / 2
			var space = get_world_2d().direct_space_state
			var query = Physics2DShapeQueryParameters.new()
			query.set_shape(select_rect)
			query.transform = Transform2D(0, (drag_end + drag_start) / 2)
			selected = space.intersect_shape(query)
			var counter = 0
			for item in selected:
				if item.collider.name != "Obstacles" && item.collider.name != "HeadQuarter":
					item.collider.select()
					counter += 1
			if counter > 0:
				emit_signal("selected", counter)

	if event is InputEventMouseMotion and dragging:
		update()

func _draw():
	if dragging:
		draw_rect(Rect2(drag_start, get_global_mouse_position() - drag_start),
				Color(.5, .5, .5), false)

func collect_resource(resource):
	for item in selected:
		if item.collider.name != "Obstacles" && item.collider.name != "HeadQuarter" and is_instance_valid(item.collider):
			item.collider.move_to(get_global_mouse_position())
			if resource == "wood":
				item.collider.set_lumberjack()
			if resource == "food":
				item.collider.set_picker()
