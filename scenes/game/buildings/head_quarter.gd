extends "res://scenes/game/buildings/base.gd"
onready var animation_player = $AnimationPlayer
onready var wood = $Wood
onready var food = $Food

func collect_wood(value):
	wood.set_resource(value)
	wood.visible = true
	animation_player.play("harvested_wood")

func collect_food(value):
	food.set_resource(value)
	food.visible = true
	animation_player.play("harvested_food")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "harvested_wood":
		wood.visible = false
