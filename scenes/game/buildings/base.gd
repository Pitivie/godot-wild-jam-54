extends StaticBody2D

export var life_point = 100
onready var life_bar = $LifeBar

signal targeted

func _ready():
	life_bar.value = life_point

func take_damage(dmg):
	$AnimationPlayer.play("damage")
	life_point -= dmg
	life_bar.value = life_point
	if life_point <= 0:
		queue_free()

func _on_Base_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		emit_signal("targeted", self)
