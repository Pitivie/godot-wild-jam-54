extends "res://scenes/game/units/unit.gd"

var resource_target = null

onready var animated_sprite = $AnimatedSprite
onready var cutting_wood = $CuttingWood
onready var pick_berry = $PickBerry

signal harvested
signal brought

enum Jobs {REST, LUMBERJACK, PICKER, MINER}

var sound_select = ["Select01","Select02","Select03"]
var sound_order = ["Ordre01","Ordre02","Ordre03","Ordre04"]
var sound_lumberjack = ["Axe01","Axe02","Axe03","Axe04"]
var sound_TreeFalling = ["TreeFalling01"]

var job = Jobs.REST

var wood = 0
var food = 0

func _ready():
	._ready()
	animated_sprite.play("idle_colon_bas_droite")

func select():
	.select()
	var sound = sound_select[randi() % sound_select.size()]
	get_node(sound).play()

func move_to(position):
	.move_to(position)
	var sound = sound_order[randi() % sound_order.size()]
	get_node(sound).play()
	if job == 0:
		stop_cutting_wood()
		stop_picking_berry()

func idle():
	.idle()
	if velocity.x < 0:
		animated_sprite.play("idle_colon_bas_gauche")
	else:
		animated_sprite.play("idle_colon_bas_droite")

func cut_wood():
	resource_target = target
	target = null
	if velocity.x < 0:
		animated_sprite.play("bucheron_colon_gauche")
	else:
		animated_sprite.play("bucheron_colon_droit")
	cutting_wood.start()
	$SoundClock.start()
	job = Jobs.REST

func pick_berry():
	resource_target = target
	target = null
	if velocity.x < 0:
		animated_sprite.play("construction_colon_bas_gauche")
	else:
		animated_sprite.play("construction_colon_bas_droite")
	pick_berry.start()
	job = Jobs.REST

func arrive():
	emit_signal("arrived")
	if target == base_position and wood > 0:
		give_wood()
	if target == base_position and food > 0:
		give_food()

	if job == Jobs.LUMBERJACK:
		cut_wood()
	elif job == Jobs.PICKER:
		pick_berry()
	else:
		idle()

func _physics_process(delta):
	if target == null:
		return
	if unit_agent.is_navigation_finished() and attack_target == null:
		arrive()
		return

	._physics_process(delta)

	if velocity.x < 0:
		animated_sprite.play("marche_colon_bas_gauche")
	else:
		animated_sprite.play("marche_colon_bas_droite")

func set_lumberjack():
	job = Jobs.LUMBERJACK

func set_picker():
	job = Jobs.PICKER

func stop_cutting_wood():
	resource_target = null
	$SoundClock.stop()
	cutting_wood.stop()

func _on_CuttingWood_timeout():
	emit_signal("harvested", resource_target)
	target = base_position
	unit_agent.set_target_location(target)
	wood = 10
	var sound = sound_TreeFalling[randi() % sound_TreeFalling.size()]
	get_node(sound).play()
	stop_cutting_wood()
	
func give_wood():
	emit_signal("brought", "wood", wood)
	wood = 0

func stop_picking_berry():
	resource_target = null

func _on_PickBerry_timeout():
	emit_signal("harvested", resource_target)
	target = base_position
	unit_agent.set_target_location(target)
	job = Jobs.REST
	food = 10
	stop_picking_berry()

func give_food():
	emit_signal("brought", "food", food)
	food = 0

func _on_SoundClock_timeout():
	var sound = sound_lumberjack[randi() % sound_lumberjack.size()]
	get_node(sound).play()
	$SoundClock.start()
