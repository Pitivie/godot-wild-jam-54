extends KinematicBody2D

var speed = 75
var life_point = 10
var attack = 2
var velocity = Vector2.ZERO

var target = null
var destination = null
var base_position = null
var attack_target = null
var curses = []

onready var life_bar = $LifeBar
onready var current_order = $CurrentOrder
onready var work_clock = $WorkClock
onready var unit_agent = $UnitAgent

signal targeted
signal arrived
signal killed

enum States {IDLE, MOVE, ATTACK}
enum Behaviors {CALME, AGRESSIVE, DEFENCIVE, FEAR}

var state = States.IDLE
var behaviors = Behaviors.CALME

func _ready():
	base_position = position

func select():
	life_bar.visible = true

func unselect():
	life_bar.visible = false

func move_to(position):
	target = position
	unit_agent.set_target_location(target)
	set_state(States.MOVE)

func idle():
	target = null
	set_state(States.IDLE)

func attack():
	set_state(States.ATTACK)
	work_clock.start()

func arrive():
	emit_signal("arrived")
	idle()

func _physics_process(delta):
	if target == null:
		return
	if unit_agent.is_navigation_finished() and attack_target == null:
		arrive()
		return

	if is_instance_valid(attack_target) && behaviors == Behaviors.AGRESSIVE:
		target = attack_target.position
		unit_agent.set_target_location(target)

	var next = unit_agent.get_next_location()
	velocity = position.direction_to(next) * speed
	velocity.y = velocity.y/2
	#unit_agent.set_velocity(velocity)
	velocity = move_and_slide(velocity)

func set_state(incoming_state):
	state = incoming_state
	if state == States.IDLE:
		current_order.text = "idle"
	if state == States.MOVE:
		current_order.text = "move"
	if state == States.ATTACK:
		current_order.text = "attack"

func take_damage(dmg):
	life_point -= dmg
	if life_point <= 0:
		emit_signal("killed")
		queue_free()

func becomes_aggressive():
	behaviors = Behaviors.AGRESSIVE
	life_bar.color = Color('#ff0000')

func becomes_defencive():
	behaviors = Behaviors.DEFENCIVE
	life_bar.color = Color('#0028ff')

func becomes_fear():
	behaviors = Behaviors.FEAR
	target = base_position
	unit_agent.set_target_location(target)
	life_bar.color = Color('#472e2e')

func becomes_calm():
	behaviors = Behaviors.CALME
	life_bar.color = Color('#ffffff')

func define_attack_target(body):
	attack_target = body
	attack_target.connect("killed", self, "_on_Farmer_killed")

func _on_Farmer_killed():
	work_clock.stop()
	attack_target = null
	target = destination
	unit_agent.set_target_location(target)
	destination = null

func _on_ContactArea_body_entered(body):
	if behaviors == Behaviors.DEFENCIVE && body.is_in_group("targetable"):
		if target == null:
			destination = position
		else:
			destination = target
		target = body.position
		unit_agent.set_target_location(target)
		define_attack_target(body)
	if body == attack_target:
		attack()

func _on_ContactArea_body_exited(body):
	if behaviors == Behaviors.DEFENCIVE && body == attack_target:
		_on_Farmer_killed()

func _on_WorkClock_timeout():
	if state == States.ATTACK:
		attack_target.take_damage(attack)

func _on_AgressiveArea_body_entered(body):
	if behaviors == Behaviors.AGRESSIVE && body.is_in_group("targetable"):
		if target == null:
			destination = position
		else:
			destination = target
		target = body.position
		unit_agent.set_target_location(target)
		define_attack_target(body)

func _on_UnitAgent_velocity_computed(safe_velocity):
	velocity = move_and_slide(safe_velocity)

func add_curse(curse):
	curses.append(curse)
	var modificators = curse.apply(self)
	for attribute in modificators:
		self.set(attribute, modificators[attribute])
