extends Sprite

#On définie l'image
const LightTexture = preload("res://assets/sprites/hud/fog_dissolve.png")
const SCOUT_SIZE = 1

var fogImage = Image.new()
var fogTexture = ImageTexture.new()
var lightImage = LightTexture.get_data()

#On repositionne la texture au centre
var light_offset = Vector2(LightTexture.get_width()/2, LightTexture.get_height()/2)

func _ready():
	#Taille de la zone de brouillard par default
	var fog_image_width = 1048
	var fog_image_height = 1048
	
	#Génération du brouillard
	fogImage.create(fog_image_width, fog_image_height, false, Image.FORMAT_RGBAH)
	fogImage.fill(Color.black)
	lightImage.convert(Image.FORMAT_RGBAH)
	scale *= SCOUT_SIZE

func update_fog(new_grid_position):
	#On verrouille les datas pour pouvoir les modifier
	fogImage.lock()
	lightImage.lock()
	
	#Fusion des image pour soustraire light à Fog
	var light_rect = Rect2(Vector2.ZERO, Vector2(lightImage.get_width(), lightImage.get_height()))
	fogImage.blend_rect(lightImage, light_rect, new_grid_position - light_offset)
	
	#On Deverrouille les datas pour ne plus y toucher
	fogImage.unlock()
	lightImage.unlock()
	update_fog_image_texture()

func update_fog_image_texture():
	#Maj de l'image
	fogTexture.create_from_image(fogImage)
	texture = fogTexture

func _input(_event):
	update_fog(get_local_mouse_position()/SCOUT_SIZE)
