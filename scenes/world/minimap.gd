extends Node2D


onready var dot    = $Ally
onready var lands  = get_node("/root/World/WorldGeneration/Lands")
onready var player = get_local_mouse_position()

func _ready():
	$FogTimer.connect("timeout", self, "update_fog")
	generate()


func generate():
	dot.position = get_local_mouse_position()/8
	$FogTimer.start()


func _process(delta):
	dot.position = get_local_mouse_position()/8


func update_fog():
	var player_pos = lands.world_to_map(get_local_mouse_position()/8)

