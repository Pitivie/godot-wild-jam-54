extends Node2D

onready var world_generation = $"../WorldGeneration"
onready var produce_unit = $"../ProduceUnit"
onready var bottom_bar = $"../BottomBar"

var obstacle : TileMap

# @todo : mutualiser avec la constante de building_preview.gd
const mapping_obstacle = {
	"barracks": 5,
	"tower": 9,
	"warehouse": 7,
	"house": 10,
	"head_quarter": 8
}

func _ready():
	obstacle = world_generation.get_node("Obstacles")

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		var map_position = obstacle.world_to_map(get_global_mouse_position())
		var tile_id = obstacle.get_cellv(map_position)/100

		for building in mapping_obstacle:
			if tile_id == mapping_obstacle[building]:
				bottom_bar.toggle_head_quarter_unit_menu()
				$SoundQG.play()
