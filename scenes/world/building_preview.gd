extends TileMap

onready var world_generation = $"../WorldGeneration"

var HeadQuarter = preload("res://scenes/game/buildings/head_quarter.tscn")
var head_quarter
var obstacle

var previous_position
var selected_building = null
var is_placable = true
var head_quater_position = Vector2(115, 115)

## Mapping between building name and preview tilemap id
const mapping_preview = {
	"barracks": 0,
	"tower": 5,
	"warehouse": 3,
	"house": 7,
}

## Mapping between building name and obstacle tilemap id
const mapping_obstacle = {
	"barracks": 5,
	"tower": 9,
	"warehouse": 7,
	"house": 10,
	"head_quarter": 8
}

## Check the size on the ground
const buildings_size = {
	"barracks": {"w": 2, "h": 2},
	"tower": {"w": 2, "h": 2},
	"warehouse": {"w": 7, "h": 4},
	"house": {"w": 4, "h": 6},
	"head_quarter": {"w": 8, "h": 8},
}

const buildings_offset = {
	"barracks": {"x": 0, "y": 0},
	"tower": {"x": 0, "y": 0},
	"warehouse": {"x": 4, "y": 1},
	"house": {"x": 3, "y": 3},
	"head_quarter": {"x": 3, "y": 3},
}

signal built

func _ready():
	obstacle = world_generation.get_node("Obstacles")
	add_building(head_quater_position, "head_quarter")

func _input(event):
	if selected_building == null:
		return

	var tilePreview = mapping_preview[selected_building]
	if event is InputEventMouseMotion:
		var map_position = obstacle.world_to_map(get_global_mouse_position())
		if previous_position != null:
			set_cellv(previous_position, -1)
		previous_position = map_position
		
		is_placable = true
		var origin = Vector2(map_position.x - buildings_offset[selected_building]["x"], map_position.y - buildings_offset[selected_building]["y"])
		for x in range(origin.x, origin.x + buildings_size[selected_building]["w"]):
			for y in range(origin.y, origin.y + buildings_size[selected_building]["h"]):
				if obstacle.get_cellv(Vector2(x, y)) >= 0:
					is_placable = false
		if is_placable == false:
			tilePreview += 1
		set_cellv(map_position, tilePreview)

	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed() and is_placable:
		emit_signal("built", selected_building)
		add_building(previous_position, selected_building)
		set_cellv(previous_position, -1)
		selected_building = null
		previous_position = null

	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		selected_building = null
		set_cellv(previous_position, -1)

func add_building(localisation, building_name):
	var origin = Vector2(localisation.x - buildings_offset[building_name]["x"], localisation.y - buildings_offset[building_name]["y"])
	for x in range(origin.x, origin.x + buildings_size[building_name]["w"]):
		for y in range(origin.y, origin.y + buildings_size[building_name]["h"]):
			obstacle.set_cell(x, y, mapping_obstacle[building_name]*100)
	if building_name == "head_quarter":
		head_quarter = HeadQuarter.instance()
		var spawn_position = obstacle.map_to_world(Vector2(localisation.x - 1, localisation.y - 1))
		head_quarter.position = spawn_position
		obstacle.add_child(head_quarter)
	else :
		obstacle.set_cellv(localisation, mapping_obstacle[building_name])

func _on_BottomBar_building_creation_pressed(building):
	selected_building = building
