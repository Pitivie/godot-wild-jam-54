extends Node

onready var top_bar = $"../TopBar"
onready var bottom_bar = $"../BottomBar"

var food = 1000
var wood = 1000
var coin = 1000

const buildind_costs = {
	"barracks": {
		"food": 0,
		"wood": 25,
		"coin": 10
	},
	"tower": {
		"food": 10,
		"wood": 100,
		"coin": 20
	},
	"warehouse": {
		"food": 30,
		"wood": 30,
		"coin": 30
	},
	"house": {
		"food": 30,
		"wood": 33,
		"coin": 40
	}
}

const unit_costs = {
	"settler": {
		"food": 5,
		"wood": 0,
		"coin": 10
	}
}

func _ready():
	bottom_bar.set_barracks_costs(buildind_costs["barracks"])
	bottom_bar.set_tower_costs(buildind_costs["tower"])
	bottom_bar.set_warehouse_costs(buildind_costs["warehouse"])
	bottom_bar.set_house_costs(buildind_costs["house"])
	bottom_bar.set_settler_costs(unit_costs["settler"])
	update_hud_stock()

func update_hud_stock():
	top_bar.update_food(food)
	top_bar.update_wood(wood)
	top_bar.update_coin(coin)
	for building in buildind_costs:
		var resources = []
		for resource in buildind_costs[building]:
			if get(resource) < buildind_costs[building][resource]:
				resources.append(resource)

			if resources.size() > 0:
				bottom_bar.disable_building(building, resources)

	for unit in unit_costs:
		var resources = []
		for resource in unit_costs[unit]:
			if get(resource) < unit_costs[unit][resource]:
				resources.append(resource)

			if resources.size() > 0:
				bottom_bar.disable_unit(unit, resources)

func _on_BuildingPreview_built(building):
	food -= buildind_costs[building]['food']
	wood -= buildind_costs[building]['wood']
	coin -= buildind_costs[building]['coin']
	update_hud_stock()

func spawnUnit(unit):
	food -= unit_costs[unit]['food']
	wood -= unit_costs[unit]['wood']
	coin -= unit_costs[unit]['coin']
	update_hud_stock()

func _on_settler_brought(resource, amount):
	var current_amount = get(resource)
	set(resource, current_amount + amount)
	update_hud_stock()
