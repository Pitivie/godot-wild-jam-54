extends Camera2D

var velocityKeyboard = Vector2.ZERO
var velocityMouse = Vector2.ZERO
var speed = 5

func _input(event):

	if event is InputEventMouseButton:
		event as InputEventMouseButton
		if event.pressed:
			match event.button_index:
				BUTTON_WHEEL_UP:
					zoom.x -= 0.1
					zoom.y -= 0.1
				BUTTON_WHEEL_DOWN:
					zoom.x += 0.1
					zoom.y += 0.1
			zoom.x = clamp(zoom.x, 0.6, 2)
			zoom.y = clamp(zoom.y, 0.6, 2)

	if event.is_action_pressed("ui_up"):
		velocityKeyboard.y = -speed
	elif event.is_action_pressed("ui_down"):
		velocityKeyboard.y = speed
	if event.is_action_released("ui_up") or event.is_action_released("ui_down"):
		velocityKeyboard.y = 0

	if event.is_action_pressed("ui_right"):
		velocityKeyboard.x = speed
	elif event.is_action_pressed("ui_left"):
		velocityKeyboard.x = -speed
	if event.is_action_released("ui_right") or event.is_action_released("ui_left"):
		velocityKeyboard.x = 0

func _physics_process(delta):
	var velocity = velocityMouse.normalized() + velocityKeyboard.normalized()
	position += velocity.normalized() * speed
	position.x = clamp(position.x, -3400, 3400)
	position.y = clamp(position.y, -1920, 1880)

func _on_Top_mouse_entered():
	velocityMouse.y = -speed

func _on_Top_mouse_exited():
	velocityMouse.y = 0

func _on_Right_mouse_entered():
	velocityMouse.x = speed

func _on_Right_mouse_exited():
	velocityMouse.x = 0

func _on_Bottom_mouse_entered():
	velocityMouse.y = speed

func _on_Bottom_mouse_exited():
	velocityMouse.y = 0

func _on_Left_mouse_entered():
	velocityMouse.x = -speed

func _on_Left_mouse_exited():
	velocityMouse.x = 0
