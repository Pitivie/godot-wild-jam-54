extends Node2D

var map_noise
var tree_noise
var map_size = Vector2(240, 240)
var camp_size = Vector2(20, 20)
var grass_cap = 0.5
var road_caps = Vector2(0.3, 0.05)
var enviroment_caps = Vector3(0.25, -0.25, -0.4)
#var enviroment_caps = Vector3(0.4, 0.3, 0.04)


func _ready():
	randomize()
	map_noise = OpenSimplexNoise.new()
	map_noise.seed = randi()
	map_noise.octaves = 24
#	noise.octaves = 1.0
	map_noise.period = 24
#	noise.period = 12
	map_noise.persistence = .7
	
	
	make_grass_map()
	make_road_map()
	make_enviroment_map()
	make_background()
	make_bruit_generation()
	
func make_grass_map():
	for x in map_size.x:
		for y in map_size.y:
			var a = map_noise.get_noise_2d(x,y)
			if a < grass_cap:
				$Lands.set_cell(x,y,0)
				
	$Lands.update_bitmask_region(Vector2(0.0, 0.0), Vector2(map_size.x, map_size.y))


#Pour tester la generation de bruit
func make_bruit_generation():
	for x in map_size.x:
		for y in map_size.y:
			var a = map_noise.get_noise_2d(x,y)
			if a > 0.4:
				$Bruit.set_cell(x,y,9)
			elif a > 0.3:
				$Bruit.set_cell(x,y,8)
			elif a > 0.2:
				$Bruit.set_cell(x,y,7)
			elif a > 0.1:
				$Bruit.set_cell(x,y,6)
			elif a > 0.0:
				$Bruit.set_cell(x,y,5)
			elif a > -0.1:
				$Bruit.set_cell(x,y,4)
			elif a > -0.2:
				$Bruit.set_cell(x,y,3)
			elif a > -0.3:
				$Bruit.set_cell(x,y,2)
			elif a > -0.4:
				$Bruit.set_cell(x,y,1)
			else:
				$Bruit.set_cell(x,y,0)
	print("Values:")
	print(map_noise.get_noise_2d(1.0, 1.0))


	$Bruit.update_bitmask_region(Vector2(0.0, 0.0), Vector2(map_size.x, map_size.y))


func make_road_map():
	for x in map_size.x:
		for y in map_size.y:
			var a = map_noise.get_noise_2d(x,y)
			if a < road_caps.x and a > road_caps.y:
				$Lands.set_cell(x,y,1)
	$Lands.update_bitmask_region(Vector2(0.0, 0.0), Vector2(map_size.x, map_size.y))

#Creation de la zone du camp
	for x in camp_size.x:
		for y in camp_size.y:
#			if a < road_caps.x and a > road_caps.y:
			
			$Lands.set_cell(map_size.x/2-camp_size.x/2+x,map_size.y/2-camp_size.y/2+y,1)
			
	$Lands.update_bitmask_region(Vector2(0.0, 0.0), Vector2(map_size.x, map_size.y))



#Pour la génération des arbres
func make_enviroment_map():
	for x in map_size.x:
		for y in map_size.y:
			var a = map_noise.get_noise_2d(x,y)
			if a < enviroment_caps.x and a > enviroment_caps.y or a < enviroment_caps.z:
				
				if (x > map_size.x/2-camp_size.x/2 and x < map_size.x/2+camp_size.x/2) and (y > map_size.y/2-camp_size.y/2 and y < map_size.y/2+camp_size.y/2):
					
					pass

				else:
					
					var chance = randi() % 6
					if chance < 2:
					
						var num = randi() % 4
						$Obstacles.set_cell(x,y, num)


			if a < enviroment_caps.x and a > enviroment_caps.y or a < enviroment_caps.z:
				
				if (x > map_size.x/2-camp_size.x/2 and x < map_size.x/2+camp_size.x/2) and (y > map_size.y/2-camp_size.y/2 and y < map_size.y/2+camp_size.y/2):
					
					pass

				else:
					
					var chance = randi() % 200
					if chance < 2:
					
						$Obstacles.set_cell(x,y, 4)
				


#Pour la corruption mais ça va être compliqué
func make_background():
	for x in map_size.x:
		for y in map_size.y:
			if $Lands.get_cell(x,y) == -1:
				if $Lands.get_cell(x,y-1) == 0:
					$Background.set_cell(x,y,0)
				
	$Background.update_bitmask_region(Vector2(0.0, 0.0), Vector2(map_size.x, map_size.y))
