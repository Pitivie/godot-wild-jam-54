extends Node2D

var Settler = preload("res://scenes/game/units/settler.tscn")
var DeerWoman = preload("res://scenes/game/units/deer_woman.tscn")

var reduce_speed_curse = preload("res://scenes/game/curses/reduce_speed.tscn")

onready var world_generation = $WorldGeneration
onready var building_preview = $BuildingPreview
onready var wakable = $Wakable
onready var stock = $Stock
onready var bottom_bar = $BottomBar
onready var select_and_order_unit = $SelectAndOrderUnit
onready var reset_spawn_index = $ResetSpawnIndex
onready var curses = $Curses
onready var curses_logic = $CursesLogic

var obstacle
var spawn_index = 0
var enemies_spawn_index = 0

func _ready():
	obstacle = world_generation.get_node("Obstacles")
	var polygon = NavigationPolygon.new()
	var outline = PoolVector2Array([Vector2(0, 0), Vector2(3840, 1920), Vector2(0, 3840), Vector2(-3840, 1920)])
	polygon.add_outline(outline)
	polygon.make_polygons_from_outlines()
	wakable.navpoly = polygon

func _on_BottomBar_unit_produced_pressed(unit_name):
	var unit = Settler.instance()

	var x = 121 + spawn_index % 8
	var y = 112 + spawn_index / 8
	var spawn_position = obstacle.map_to_world(Vector2(x, y))
	spawn_index += 1

	unit.position = spawn_position # Vector2(53, 1937)
	unit.connect("brought", stock, "_on_settler_brought")
	unit.connect("brought", self, "_on_settler_brought")
	unit.connect("harvested", self, "_on_settler_harvested")
	unit.add_to_group('player_unit')
	for curse in curses_logic.get_children():
		unit.add_curse(curse)
	obstacle.add_child(unit)
	stock.spawnUnit(unit_name)
	unit.base_position = Vector2(53, 1900)
	reset_spawn_index.start()

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		var map_position = obstacle.world_to_map(get_global_mouse_position())
		if obstacle.get_cellv(map_position) == 1:
			select_and_order_unit.collect_resource("wood")
		if obstacle.get_cellv(map_position) == 4:
			select_and_order_unit.collect_resource("food")

func _on_settler_brought(resource, amount):
	if resource == "wood":
		building_preview.head_quarter.collect_wood(amount)
	if resource == "food":
		building_preview.head_quarter.collect_food(amount)

func _on_settler_harvested(position):
	var map_position = obstacle.world_to_map(position)
	if obstacle.get_cellv(map_position) == 4:
		obstacle.set_cellv(map_position, 3)
	if obstacle.get_cellv(map_position) == 1:
		obstacle.set_cellv(map_position, 11)

func _on_ResetSpawnIndex_timeout():
	spawn_index = 0

func spawn_enemies(enemy, initial_tile_position, quanity):
	for enemies_spawn_index in range(0, quanity):
		var unit = enemy.instance()
		var x = initial_tile_position.x + enemies_spawn_index % 8
		var y = initial_tile_position.y + enemies_spawn_index / 8
		var spawn_position = obstacle.map_to_world(Vector2(x, y))

		unit.position = spawn_position
		obstacle.add_child(unit)
		unit.attack_target = building_preview.head_quarter
		unit.target = building_preview.head_quarter.position
		unit.becomes_aggressive()

func _on_Wave1_timeout():
	spawn_enemies(DeerWoman, Vector2(125, 125), 30)

func invok_curse():
	var curse = reduce_speed_curse.instance()
	curses_logic.add_child(curse)
	for unit in get_tree().get_nodes_in_group('player_unit'):
		unit.add_curse(curse)
	curses.add_curse(curse.get_menu())
