extends KinematicBody2D

export var speed = 100
export var life_point = 10
export var attack = 2
export var path_to_base : NodePath
var base : StaticBody2D
var velocity = Vector2.ZERO

var target = null
var destination = null
var base_position = null
var attack_target = null
var resource_target = null

onready var life_bar = $LifeBar
onready var farmer = $Farmer
onready var current_order = $CurrentOrder
onready var work_clock = $WorkClock
onready var farmer_agent = $FarmerAgent
onready var animated_sprite = $AnimatedSprite
onready var cutting_wood = $CuttingWood
onready var pick_berry = $PickBerry

signal targeted
signal arrived
signal killed
signal harvested
signal brought

enum States {IDLE, MOVE, ATTACK}
enum Jobs {REST, LUMBERJACK, PICKER, MINER}
enum Behaviors {CALME, AGRESSIVE, DEFENCIVE, FEAR}

var state = States.IDLE
var behaviors = Behaviors.CALME
var job = Jobs.REST

var wood = 0
var food = 0

func _ready():
	animated_sprite.play("idle_colon_bas_droite")
	base = get_node(path_to_base)
	if base != null:
		base_position = base.position
	else:
		base_position = position

func select():
	life_bar.visible = true

func unselect():
	life_bar.visible = false

func move_to(position):
	target = position
	farmer_agent.set_target_location(target)
	set_state(States.MOVE)

func idle():
	target = null
	if velocity.x < 0:
		animated_sprite.play("idle_colon_bas_gauche")
	else:
		animated_sprite.play("idle_colon_bas_droite")
	set_state(States.IDLE)

func cut_wood():
	resource_target = target
	target = null
	if velocity.x < 0:
		animated_sprite.play("bucheron_colon_gauche")
	else:
		animated_sprite.play("bucheron_colon_droit")
	cutting_wood.start()

func pick_berry():
	resource_target = target
	target = null
	if velocity.x < 0:
		animated_sprite.play("construction_colon_bas_gauche")
	else:
		animated_sprite.play("construction_colon_bas_droite")
	pick_berry.start()

func attack():
	set_state(States.ATTACK)
	work_clock.start()

func _physics_process(delta):
	if target == null:
		return
	if farmer_agent.is_navigation_finished() and attack_target == null:
		emit_signal("arrived")
		if target == base_position and wood > 0:
			give_wood()
		if target == base_position and food > 0:
			give_food()
		if job == Jobs.LUMBERJACK:
			cut_wood()
		elif job == Jobs.PICKER:
			pick_berry()
		else:
			idle()
		return

	if is_instance_valid(attack_target) && behaviors == Behaviors.AGRESSIVE:
		target = attack_target.position
		farmer_agent.set_target_location(target)

	var next = farmer_agent.get_next_location()
	velocity = position.direction_to(next) * speed
	#farmer_agent.set_velocity(velocity)
	velocity = move_and_slide(velocity)

	if velocity.x < 0:
		animated_sprite.play("marche_colon_bas_gauche")
	else:
		animated_sprite.play("marche_colon_bas_droite")

func set_state(incoming_state):
	state = incoming_state
	if state == States.IDLE:
		current_order.text = "idle"
	if state == States.MOVE:
		current_order.text = "move"
	if state == States.ATTACK:
		current_order.text = "attack"

func _on_Farmer_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		emit_signal("targeted", self)

func _on_ContactArea_body_entered(body):
	if behaviors == Behaviors.DEFENCIVE && body.is_in_group("targetable"):
		if target == null:
			destination = position
		else:
			destination = target
		target = body.position
		farmer_agent.set_target_location(target)
		define_attack_target(body)
	if body == attack_target:
		attack()

func _on_WorkClock_timeout():
	if state == States.ATTACK:
		attack_target.take_damage(attack)

func take_damage(dmg):
	$AnimationPlayer.play("damage")
	life_point -= dmg
	if life_point <= 0:
		emit_signal("killed")
		queue_free()

func _on_AgressiveArea_body_entered(body):
	if behaviors == Behaviors.AGRESSIVE && body.is_in_group("targetable"):
		if target == null:
			destination = position
		else:
			destination = target
		target = body.position
		farmer_agent.set_target_location(target)
		define_attack_target(body)

func becomes_aggressive():
	behaviors = Behaviors.AGRESSIVE
	life_bar.color = Color('#ff0000')

func becomes_defencive():
	behaviors = Behaviors.DEFENCIVE
	life_bar.color = Color('#0028ff')

func becomes_fear():
	behaviors = Behaviors.FEAR
	target = base_position
	farmer_agent.set_target_location(target)
	life_bar.color = Color('#472e2e')

func becomes_calm():
	behaviors = Behaviors.CALME
	life_bar.color = Color('#ffffff')

func define_attack_target(body):
	attack_target = body
	attack_target.connect("killed", self, "_on_Farmer_killed")

func _on_Farmer_killed():
	work_clock.stop()
	attack_target = null
	target = destination
	farmer_agent.set_target_location(target)
	destination = null

func _on_ContactArea_body_exited(body):
	if behaviors == Behaviors.DEFENCIVE && body == attack_target:
		_on_Farmer_killed()

func _on_FarmerAgent_velocity_computed(safe_velocity):
	velocity = move_and_slide(safe_velocity)

func set_lumberjack():
	job = Jobs.LUMBERJACK

func set_picker():
	job = Jobs.PICKER

func _on_CuttingWood_timeout():
	emit_signal("harvested", resource_target)
	resource_target = null
	target = base_position
	farmer_agent.set_target_location(target)
	job = Jobs.REST
	wood = 10

func give_wood():
	emit_signal("brought", "wood", wood)
	wood = 0

func _on_PickBerry_timeout():
	emit_signal("harvested", resource_target)
	resource_target = null
	target = base_position
	farmer_agent.set_target_location(target)
	job = Jobs.REST
	food = 10


func give_food():
	emit_signal("brought", "food", food)
	food = 0
