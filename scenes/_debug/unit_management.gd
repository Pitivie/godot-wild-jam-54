extends Node2D

onready var spawn = $Spawn
onready var agressive_toggle = $AgressiveToggle
onready var defencive_toggle = $DefenciveToggle
onready var fear_toggle = $FearToggle

var dragging = false
var selected = []
var drag_start = Vector2.ZERO
var select_rect = RectangleShape2D.new()

func _ready():
	var targetable = get_tree().get_nodes_in_group("targetable")
	for item in targetable:
		item.connect("targeted", self, "_on_Farmer_targeted")

	var selectable = get_tree().get_nodes_in_group("selectable")
	for item in selectable:
		item.connect("arrived", self, "_on_Farmer_arrived")

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and selected.size() > 0 and event.is_pressed():
		for item in selected:
			if is_instance_valid(item.collider):
				item.collider.move_to(event.position)
				item.collider.attack_target = null
		spawn.position = event.position
		spawn.visible = true

	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			spawn.visible = false
			for item in selected:
				item.collider.unselect()
			selected = []
			dragging = true
			drag_start = event.position
		elif dragging:
			dragging = false
			update()
			var drag_end = event.position
			select_rect.extents = (drag_end - drag_start) / 2
			var space = get_world_2d().direct_space_state
			var query = Physics2DShapeQueryParameters.new()
			query.set_exclude(get_tree().get_nodes_in_group('stand'))
			query.set_shape(select_rect)
			query.transform = Transform2D(0, (drag_end + drag_start) / 2)
			selected = space.intersect_shape(query)
			for item in selected:
				item.collider.select()
	if event is InputEventMouseMotion and dragging:
		update()

func _draw():
	if dragging:
		draw_rect(Rect2(drag_start, get_global_mouse_position() - drag_start),
				Color(.5, .5, .5), false)

func _on_Farmer_targeted(body):
	for item in selected:
		item.collider.attack_target = body

func _on_Farmer_arrived():
	spawn.visible = false

func _on_AgressiveToggle_toggled(button_pressed):
	if button_pressed:
		defencive_toggle.pressed = false
		fear_toggle.pressed = false
		spawn.modulate = Color('#ff0000')
		for item in selected:
			item.collider.becomes_aggressive()
	else:
		spawn.modulate = Color('#14ff00')
		for item in selected:
			item.collider.becomes_calm()

func _on_DefenciveToggle_toggled(button_pressed):
	if button_pressed:
		agressive_toggle.pressed = false
		fear_toggle.pressed = false
		spawn.modulate = Color('#0028ff')
		for item in selected:
			item.collider.becomes_defencive()
	else:
		spawn.modulate = Color('#14ff00')
		for item in selected:
			item.collider.becomes_calm()

func _on_FearToggle_toggled(button_pressed):
	if button_pressed:
		agressive_toggle.pressed = false
		defencive_toggle.pressed = false
		for item in selected:
			item.collider.becomes_fear()
	else:
		spawn.modulate = Color('#14ff00')
		for item in selected:
			item.collider.becomes_calm()

func _on_PauseButton_pressed():
	$Enemie.get_tree().paused = true
	$pause_popup.show()

func _on_AcceptDialog_confirmed():
	$pause_popup.hide()
	get_tree().paused = false
